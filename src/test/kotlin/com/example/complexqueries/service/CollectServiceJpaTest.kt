package com.example.complexqueries.service

import com.example.complexqueries.model.Collect
import com.example.complexqueries.model.CollectStatus
import com.example.complexqueries.repository.CollectCriteriaDao
import com.example.complexqueries.repository.UserRepository
import org.h2.tools.Server
import org.junit.Before
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager
import org.springframework.context.annotation.Import
import kotlin.test.assertTrue


@ExtendWith(SpringExtension::class)
@Import(CollectService::class, CollectCriteriaDao::class)
@DataJpaTest
class CollectServiceJpaTest(@Autowired val sut: CollectService,
                             @Autowired val entityManager: TestEntityManager,
                            @Autowired val userRepository: UserRepository
                            ) {

    @Test
    fun `getting validated collects for anna returns only appropriate items`() {
        // arrange
        val anyuser = userRepository.findBySubject("anyuser")!!
        val leo = userRepository.findBySubject("leo")!!
        val anna = userRepository.findBySubject("anna")!!
        val joao = userRepository.findBySubject("joao")!!
        val beatrix = userRepository.findBySubject("beatrix")!!

        val collectBase = Collect(requester = anyuser, status = null)
        val manyCollects = (1..20).map { collectBase.copy(status = CollectStatus.values()[it % CollectStatus.values().size]) }

        manyCollects.forEach { entityManager.persist(it) }

        val leoValidatedCollect1 = collectBase.copy(requester = leo, status = CollectStatus.VALIDATED)
        val leoValidatedCollect2 = collectBase.copy(requester = leo, status = CollectStatus.VALIDATED)
        val leoCancelledCollect = collectBase.copy(requester = leo, status = CollectStatus.CANCELLED)
        val joaoValidatedCollect = collectBase.copy(requester = joao, status = CollectStatus.VALIDATED)
        val joaoCancelledCollect = collectBase.copy(requester = joao, status = CollectStatus.CANCELLED)
        val beatrixValidatedCollect = collectBase.copy(requester = beatrix, status = CollectStatus.VALIDATED)
        val beatrixCancelledCollect = collectBase.copy(requester = beatrix, status = CollectStatus.CANCELLED)
        val collects = listOf(leoValidatedCollect1, leoValidatedCollect2, leoCancelledCollect, joaoCancelledCollect, joaoValidatedCollect, beatrixValidatedCollect, beatrixCancelledCollect)
        collects.forEach { entityManager.persist(it) }

        // act
        val validatedCollectsForAnna = sut.getValidatedCollectsInCollectorArea(anna)

        // assert
        assertEquals(0, validatedCollectsForAnna.filter { it.status != CollectStatus.VALIDATED }.size, "Only validated collects should be returned")
        assertEquals(3, validatedCollectsForAnna.size, "Only 3 validated collect in DB for anna")
        assertTrue("Collects are from leo or beatrix") { validatedCollectsForAnna.map { it.requester in listOf(leo, beatrix) }.reduce{a, b -> a && b } }
        assertTrue("Collects the correct ones") { validatedCollectsForAnna.map { it in listOf(leoValidatedCollect1, leoValidatedCollect2, beatrixValidatedCollect) }.reduce{a, b -> a && b } }

    }

    // Usefull for debug to have access to the H2 Console. Place it at the start of the test you want to debug
    fun initTest(){
        val webServer = Server.createWebServer("-web",
                                        "-webAllowOthers", "-webPort", "8082");
        webServer.start();
    }

    @Test
    fun `getting validated collects for anna returns only appropriate items with JPQL`() {
        // arrange
        val anyuser = userRepository.findBySubject("anyuser")!!
        val leo = userRepository.findBySubject("leo")!!
        val anna = userRepository.findBySubject("anna")!!
        val joao = userRepository.findBySubject("joao")!!
        val beatrix = userRepository.findBySubject("beatrix")!!

        val collectBase = Collect(requester = anyuser, status = null)
        val manyCollects = (1..20).map { collectBase.copy(status = CollectStatus.values()[it % CollectStatus.values().size]) }

        manyCollects.forEach { entityManager.persist(it) }

        val leoValidatedCollect1 = collectBase.copy(requester = leo, status = CollectStatus.VALIDATED)
        val leoValidatedCollect2 = collectBase.copy(requester = leo, status = CollectStatus.VALIDATED)
        val leoCancelledCollect = collectBase.copy(requester = leo, status = CollectStatus.CANCELLED)
        val joaoValidatedCollect = collectBase.copy(requester = joao, status = CollectStatus.VALIDATED)
        val joaoCancelledCollect = collectBase.copy(requester = joao, status = CollectStatus.CANCELLED)
        val beatrixValidatedCollect = collectBase.copy(requester = beatrix, status = CollectStatus.VALIDATED)
        val beatrixCancelledCollect = collectBase.copy(requester = beatrix, status = CollectStatus.CANCELLED)
        val collects = listOf(leoValidatedCollect1, leoValidatedCollect2, leoCancelledCollect, joaoCancelledCollect, joaoValidatedCollect, beatrixValidatedCollect, beatrixCancelledCollect)
        collects.forEach { entityManager.persist(it) }

        // act
        val validatedCollectsForAnna = sut.getValidatedCollectsInCollectorAreaJPQL(anna)

        // assert
        assertEquals(0, validatedCollectsForAnna.filter { it.status != CollectStatus.VALIDATED }.size, "Only validated collects should be returned")
        assertEquals(3, validatedCollectsForAnna.size, "Only 3 validated collect in DB for anna")
        assertTrue("Collects are from leo or beatrix") { validatedCollectsForAnna.map { it.requester in listOf(leo, beatrix) }.reduce{a, b -> a && b } }
        assertTrue("Collects the correct ones") { validatedCollectsForAnna.map { it in listOf(leoValidatedCollect1, leoValidatedCollect2, beatrixValidatedCollect) }.reduce{a, b -> a && b } }

    }

    @Test
    fun `getting validated collects for anna returns only appropriate items with Criteria`() {
        // arrange
        val anyuser = userRepository.findBySubject("anyuser")!!
        val leo = userRepository.findBySubject("leo")!!
        val anna = userRepository.findBySubject("anna")!!
        val joao = userRepository.findBySubject("joao")!!
        val beatrix = userRepository.findBySubject("beatrix")!!

        val collectBase = Collect(requester = anyuser, status = null)
        val manyCollects = (1..20).map { collectBase.copy(status = CollectStatus.values()[it % CollectStatus.values().size]) }

        manyCollects.forEach { entityManager.persist(it) }

        val leoValidatedCollect1 = collectBase.copy(requester = leo, status = CollectStatus.VALIDATED)
        val leoValidatedCollect2 = collectBase.copy(requester = leo, status = CollectStatus.VALIDATED)
        val leoCancelledCollect = collectBase.copy(requester = leo, status = CollectStatus.CANCELLED)
        val joaoValidatedCollect = collectBase.copy(requester = joao, status = CollectStatus.VALIDATED)
        val joaoCancelledCollect = collectBase.copy(requester = joao, status = CollectStatus.CANCELLED)
        val beatrixValidatedCollect = collectBase.copy(requester = beatrix, status = CollectStatus.VALIDATED)
        val beatrixCancelledCollect = collectBase.copy(requester = beatrix, status = CollectStatus.CANCELLED)
        val collects = listOf(leoValidatedCollect1, leoValidatedCollect2, leoCancelledCollect, joaoCancelledCollect, joaoValidatedCollect, beatrixValidatedCollect, beatrixCancelledCollect)
        collects.forEach { entityManager.persist(it) }

        // act
        val validatedCollectsForAnna = sut.getValidatedCollectsInCollectorAreaCriteria(anna)

        // assert
        assertEquals(0, validatedCollectsForAnna.filter { it.status != CollectStatus.VALIDATED }.size, "Only validated collects should be returned")
        assertEquals(3, validatedCollectsForAnna.size, "Only 3 validated collect in DB for anna")
        assertTrue("Collects are from leo or beatrix") { validatedCollectsForAnna.map { it.requester in listOf(leo, beatrix) }.reduce{a, b -> a && b } }
        assertTrue("Collects the correct ones") { validatedCollectsForAnna.map { it in listOf(leoValidatedCollect1, leoValidatedCollect2, beatrixValidatedCollect) }.reduce{a, b -> a && b } }

    }
}
