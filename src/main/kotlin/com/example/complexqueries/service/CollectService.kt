package com.example.complexqueries.service

import com.example.complexqueries.model.Collect
import com.example.complexqueries.model.User
import com.example.complexqueries.repository.CollectCriteriaDao
import com.example.complexqueries.repository.CollectRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CollectService(@Autowired val collectRepository: CollectRepository,
                     @Autowired val criteria: CollectCriteriaDao) {

    fun getValidatedCollectsInCollectorArea(collector: User): List<Collect> {
        return collectRepository.findValidatedCollectsInCollectorArea(collector_id = collector.id!!)
    }

    fun getValidatedCollectsInCollectorAreaJPQL(collector: User): List<Collect> {
        return collectRepository.findValidatedCollectsInCollectorAreaJPQL(collector_id = collector.id!!)
    }

    fun getValidatedCollectsInCollectorAreaCriteria(collector: User): List<Collect> {
        return criteria.findValidatedCollectsInCollectorAreaCriteria(collector_id = collector.id!!)
    }



}

