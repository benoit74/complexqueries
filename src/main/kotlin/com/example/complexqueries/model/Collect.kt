package com.example.complexqueries.model

import javax.persistence.*

@Entity
data class Collect(

        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name="requester_id")
        val requester : User,

        @Enumerated(EnumType.STRING)
        val status : CollectStatus?

) : AbstractEntity<Int>()

