package com.example.complexqueries.model

import javax.persistence.*

@Entity
data class User(

        @Column(unique = true)
        val subject: String,

        @ManyToOne(fetch = FetchType.EAGER)
        @JoinColumn(name="area_id")
        val area : Area?,

        @ManyToMany(fetch = FetchType.EAGER)
        val collectionArea : Set<Area>?

) : AbstractEntity<Int>()
