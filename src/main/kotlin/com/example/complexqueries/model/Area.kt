package com.example.complexqueries.model

import javax.persistence.Entity

/*
 A geographical area, grouping user into recycling zones.
 A requester is assigned a single area where its waste must be sent.
 A collector might be allowed to collect multiple areas (typically for businesses)
 */
@Entity
data class Area (

        val name: String

) : AbstractEntity<Int>()
