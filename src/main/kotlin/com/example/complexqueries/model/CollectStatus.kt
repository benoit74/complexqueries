package com.example.complexqueries.model

enum class CollectStatus(val value: String) {
    VALIDATED("validated"),
    CANCELLED("cancelled");

}
