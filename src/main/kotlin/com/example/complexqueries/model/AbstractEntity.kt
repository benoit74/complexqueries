package com.example.complexqueries.model

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import org.springframework.data.util.ProxyUtils
import java.time.LocalDateTime
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.MappedSuperclass

/**
 * Abstract base class for entities. Allows parameterization of id type, chooses auto-generation and implements
 * [equals] and [hashCode] based on that id.
 *
 * This class was inspired by [org.springframework.data.jpa.domain.AbstractPersistable], which is part of the Spring Data project.
 */

@MappedSuperclass
abstract class AbstractEntity<T> (
        @Id
        @GeneratedValue
        var id: T? = null,

        @CreationTimestamp
        var createDate: LocalDateTime? = null,

        @UpdateTimestamp
        var updateDate: LocalDateTime? = null

) {
    override fun equals(other: Any?): Boolean {
        other ?: return false

        if (this === other) return true

        if (javaClass != ProxyUtils.getUserClass(other)) return false

        other as AbstractEntity<*>

        return if (null == this.id) false else this.id == other.id
    }

    // see: https://kotlinexpertise.com/hibernate-with-kotlin-spring-boot/
    // technical choice, we will be rich when this becomes a problem
    override fun hashCode(): Int {
        return 31
    }
}
