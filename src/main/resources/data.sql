-- noinspection SqlNoDataSourceInspectionForFile

INSERT INTO AREA (id, name) VALUES (1, 'New York')
INSERT INTO AREA (id, name) VALUES (2, 'Boston')
INSERT INTO AREA (id, name) VALUES (3, 'Chicago')
INSERT INTO AREA (id, name) VALUES (4, 'San Francisco')

INSERT INTO USER (id, subject, area_id) VALUES (1, 'anna', 1)
INSERT INTO USER (id, subject, area_id) VALUES (2, 'leo', 1)
INSERT INTO USER (id, subject, area_id) VALUES (3, 'joao', 2)
INSERT INTO USER (id, subject, area_id) VALUES (4, 'beatrix', 3)
INSERT INTO USER (id, subject, area_id) VALUES (5, 'anyuser', 4)

INSERT INTO USER_COLLECTION_AREA (user_id, collection_area_id) VALUES (1, 1)
INSERT INTO USER_COLLECTION_AREA (user_id, collection_area_id) VALUES (1, 3)


